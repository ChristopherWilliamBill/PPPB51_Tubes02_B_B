package com.pppb.travelindo.Model;

import org.parceler.Parcel;

@Parcel
public class Course {

    private String course_id;
    private String source;
    private String destination;
    private String datetime;
    private String vehicle;
    private int num_seats;
    private int[] seats;
    private int fee;

    public Course(){}

    public Course(String course_id, String source, String destination, String datetime, String vehicle, int num_seats, int[] seats, int fee) {
        this.course_id = course_id;
        this.source = source;
        this.destination = destination;
        this.datetime = datetime;
        this.vehicle = vehicle;
        this.num_seats = num_seats;
        this.seats = seats;
        this.fee = fee;
    }

    public String getCourse_id() {
        return course_id;
    }

    public void setCourse_id(String course_id) {
        this.course_id = course_id;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public String getDatetime() {
        return datetime;
    }

    public void setDatetime(String datetime) {
        this.datetime = datetime;
    }

    public String getVehicle() {
        return vehicle;
    }

    public void setVehicle(String vehicle) {
        this.vehicle = vehicle;
    }

    public int getNum_seats() {
        return num_seats;
    }

    public void setNum_seats(int num_seats) {
        this.num_seats = num_seats;
    }

    public int[] getSeats() {
        return seats;
    }

    public void setSeats(int[] seats) {
        this.seats = seats;
    }

    public int getFee() {
        return fee;
    }

    public void setFee(int fee) {
        this.fee = fee;
    }
}
