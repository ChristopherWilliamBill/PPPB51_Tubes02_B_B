package com.pppb.travelindo.Model;

import org.parceler.Parcel;

@Parcel
public class HasilOrder {
    private String order_id;
    private int[] seats;

    public HasilOrder(){}

    public HasilOrder(String order_id, int[] seats) {
        this.order_id = order_id;
        this.seats = seats;
    }

    public String getOrder_id() {
        return order_id;
    }

    public void setOrder_id(String order_id) {
        this.order_id = order_id;
    }

    public int[] getSeats() {
        return seats;
    }

    public void setSeats(int[] seats) {
        this.seats = seats;
    }
}
