package com.pppb.travelindo.Model;

import com.pppb.travelindo.Model.HasilOrder;

import java.util.List;

public class Order {

    private String message;
    private List<HasilOrder> payload;

    public Order(String message, List<HasilOrder> payload) {
        this.message = message;
        this.payload = payload;
    }

    public String getMessage() {
        return message;
    }

    public void setMessage(String message) {
        this.message = message;
    }

    public List<HasilOrder> getPayload() {
        return payload;
    }

    public void setPayload(List<HasilOrder> payload) {
        this.payload = payload;
    }
}
