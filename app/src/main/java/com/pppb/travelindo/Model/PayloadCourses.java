package com.pppb.travelindo.Model;

import com.google.gson.annotations.SerializedName;
import com.pppb.travelindo.Model.Course;

import java.util.List;

public class PayloadCourses {
    @SerializedName("payload")
    private List<Course> courses;

    public PayloadCourses(List<Course> courses) {
        this.courses = courses;
    }

    public List<Course> getCourses() {
        return courses;
    }

    public void setCourses(List<Course> courses) {
        this.courses = courses;
    }
}
