package com.pppb.travelindo.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PayloadHistory {
    @SerializedName("payload")
    private List<History> histories;

    public List<History> getHistories() {
        return histories;
    }

    public void setHistories(List<History> histories) {
        this.histories = histories;
    }
}
