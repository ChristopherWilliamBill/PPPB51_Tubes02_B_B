package com.pppb.travelindo.Model;

import com.google.gson.annotations.SerializedName;

import java.util.List;

public class PayloadRoutes {
    @SerializedName("payload")
    private List<Route> routes;

    public PayloadRoutes(List<Route> routes) {
        this.routes = routes;
    }

    public List<Route> getRoutes() {
        return routes;
    }

    public void setRoutes(List<Route> routes) {
        this.routes = routes;
    }
}
