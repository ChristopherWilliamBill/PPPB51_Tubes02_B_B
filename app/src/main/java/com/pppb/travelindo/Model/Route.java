package com.pppb.travelindo.Model;

public class Route {

    private String source;
    private String destination;
    private int fee;

    public Route(String source, String destination, int fee) {
        this.source = source;
        this.destination = destination;
        this.fee = fee;
    }

    public String getSource() {
        return source;
    }

    public void setSource(String source) {
        this.source = source;
    }

    public String getDestination() {
        return destination;
    }

    public void setDestination(String destination) {
        this.destination = destination;
    }

    public int getFee() {
        return fee;
    }

    public void setFee(int fee) {
        this.fee = fee;
    }
}
