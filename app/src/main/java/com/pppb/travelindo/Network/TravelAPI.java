package com.pppb.travelindo.Network;

import com.pppb.travelindo.Model.Order;
import com.pppb.travelindo.Model.PayloadCourses;
import com.pppb.travelindo.Model.PayloadHistory;
import com.pppb.travelindo.Model.PayloadRoutes;
import com.pppb.travelindo.Model.User;

import retrofit2.Call;
import retrofit2.http.Field;
import retrofit2.http.FormUrlEncoded;
import retrofit2.http.GET;
import retrofit2.http.Header;
import retrofit2.http.POST;
import retrofit2.http.Query;

public interface TravelAPI {

    @FormUrlEncoded
    @POST("authenticate")
    Call<User> logIn(
            @Field("username") String username,
            @Field("password") String password
    );

    @GET("routes")
    Call<PayloadRoutes> getRoutes(@Header("Authorization") String header);

    @GET("courses")
    Call<PayloadCourses> getCourses(
            @Header("Authorization") String header,
            @Query("source") String source,
            @Query("destination") String destination,
            @Query("vehicle") String vehicle,
            @Query("date") String date,
            @Query("hour") String hour
            );

    @FormUrlEncoded
    @POST("orders")
    Call<Order> addNewOrder(
            @Header("Authorization") String header,
            @Field("course_id") String course_id,
            @Field("seats") String seats
    );

    @GET("orders")
    Call<PayloadHistory> getOrders(
            @Header("Authorization") String header,
            @Query("limit") int limit,
            @Query("offset") int offset
    );
}
