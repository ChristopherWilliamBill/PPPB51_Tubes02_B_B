package com.pppb.travelindo.View;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.Nullable;
import androidx.fragment.app.DialogFragment;

import com.appeaser.sublimepickerlibrary.SublimePicker;
import com.appeaser.sublimepickerlibrary.datepicker.SelectedDate;
import com.appeaser.sublimepickerlibrary.helpers.SublimeListenerAdapter;
import com.appeaser.sublimepickerlibrary.helpers.SublimeOptions;
import com.appeaser.sublimepickerlibrary.recurrencepicker.SublimeRecurrencePicker;
import com.pppb.travelindo.R;

import java.text.DateFormat;
import java.util.Locale;
import java.util.TimeZone;

/* References :
 https://github.com/vikramkakkar/SublimePicker#readme
 https://fabcoding.com/2019/03/14/create-a-2-in-1-date-time-picker-using-sublimepicker/
*/
public class DateTimePickerFragment extends DialogFragment {
    DateFormat mDateFormat, mTimeFormat;
    SublimePicker sublimePicker;
    Callback mCallback;

    SublimeListenerAdapter mListener = new SublimeListenerAdapter() {
        @Override
        public void onDateTimeRecurrenceSet(SublimePicker sublimeMaterialPicker, SelectedDate selectedDate, int hourOfDay, int minute, SublimeRecurrencePicker.RecurrenceOption recurrenceOption, String recurrenceRule) {
            if (mCallback != null) {
                mCallback.onDateTimeReccurenceSet(selectedDate, hourOfDay, minute, recurrenceOption, recurrenceRule);
                dismiss();
            }
            Log.d("tes", mCallback+"");
        }

        @Override
        public void onCancelled() {
            if(mCallback != null){
                mCallback.onCancelled();
            }
            dismiss();
        }

        public void onOkay() {
            dismiss();
        }
    };

    public DateTimePickerFragment() {
        this.mDateFormat = DateFormat.getDateInstance(DateFormat.MEDIUM, Locale.getDefault());
        this.mTimeFormat = DateFormat.getTimeInstance(DateFormat.SHORT, Locale.getDefault());
        this.mTimeFormat.setTimeZone(TimeZone.getTimeZone("GMT+7"));
    }

    public void setCallback(Callback callback) {
        mCallback = callback;
    }

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.sublimePicker = (SublimePicker) getActivity().getLayoutInflater().inflate(R.layout.sublime_picker, container);
        Bundle args = getArguments();
        SublimeOptions opt = null;
        if (args != null){
            opt = args.getParcelable("SUBLIME_OPTIONS");
        }
        this.sublimePicker.initializePicker(opt, mListener);
        return this.sublimePicker;
    }

    public interface Callback {
        void onCancelled();
        void onDateTimeReccurenceSet(SelectedDate selectedDate, int hourOfDay, int minute, SublimeRecurrencePicker.RecurrenceOption recurrenceOption, String recurrenceRule);
        void onOkay();
    }
}
