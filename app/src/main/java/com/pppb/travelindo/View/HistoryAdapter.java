package com.pppb.travelindo.View;

import android.app.Activity;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;

import com.pppb.travelindo.Model.Course;
import com.pppb.travelindo.Model.History;
import com.pppb.travelindo.databinding.ForLvHistoriBinding;

import java.util.ArrayList;

public class HistoryAdapter extends BaseAdapter {
    private ForLvHistoriBinding binding;
    private Activity activity;
    private ArrayList<History> historyArrayList;

    public HistoryAdapter(Activity activity, ArrayList<History> historyArrayList) {
        this.activity = activity;
        this.historyArrayList = historyArrayList;
    }

    @Override
    public int getCount() {
        return historyArrayList.size();
    }

    @Override
    public History getItem(int i) {
        return historyArrayList.get(i);
    }

    @Override
    public long getItemId(int i) {
        return 0;
    }

    @Override
    public View getView(int i, View convertView, ViewGroup viewGroup) {
        binding = ForLvHistoriBinding.inflate(LayoutInflater.from(this.activity), viewGroup, false);

        ViewHolder viewHolder;

        if(convertView == null){
            convertView = binding.getRoot();
            viewHolder = new ViewHolder(binding, i);
            convertView.setTag(viewHolder);
        }else{
            viewHolder = (ViewHolder) convertView.getTag();
        }

        viewHolder.updateView(this.getItem(i));
        return convertView;
    }

    private class ViewHolder{
        protected ForLvHistoriBinding binding;
        private int i;

        public ViewHolder(ForLvHistoriBinding binding, int i){
            this.binding = binding;
            this.i = i;
        }

        public void updateView(History history){
            String seats = "Seats: ";
            for(int i = 0; i < history.getSeats().length; i++){
                if(i == history.getSeats().length - 1){
                    seats += history.getSeats()[i];
                }else{
                    seats += history.getSeats()[i] + ", ";
                }
            }
            this.binding.SourceDestination.setText(history.getSource() + " to " + history.getDestination());
            this.binding.tvTanggal.setText(history.getCourse_datetime());
            this.binding.tvFee.setText("Rp " + history.getFee());
            this.binding.tvSeats.setText(seats);
        }
    }
}

