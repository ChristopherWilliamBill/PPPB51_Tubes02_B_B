package com.pppb.travelindo.View;

import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.ListView;

import androidx.annotation.NonNull;
import androidx.databinding.ViewDataBinding;
import androidx.fragment.app.Fragment;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;
import androidx.recyclerview.widget.RecyclerView;

import com.pppb.travelindo.Model.Course;
import com.pppb.travelindo.Model.History;
import com.pppb.travelindo.Model.PayloadCourses;
import com.pppb.travelindo.Model.PayloadHistory;
import com.pppb.travelindo.Model.PayloadRoutes;
import com.pppb.travelindo.Network.TravelAPI;
import com.pppb.travelindo.ViewModel.TravelindoViewModel;
import com.pppb.travelindo.databinding.HistoryFragmentBinding;

import java.util.ArrayList;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HistoryFragment extends Fragment{
    private TravelindoViewModel travelindoViewModel;
    private HistoryFragmentBinding binding;
    private String token;
    private ArrayList<History> historyArrayList;
    private int limit;
    private int offset;

    public HistoryFragment(){ }

    public static HistoryFragment newInstance(String token){
        HistoryFragment historyFragment = new HistoryFragment();
        Bundle args = new Bundle();
        args.putString("token", token);

        historyFragment.setArguments(args);
        return historyFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        this.binding = HistoryFragmentBinding.inflate(inflater, container, false);
        View view = this.binding.getRoot();
        this.token = this.getArguments().getString("token");
        this.limit = 10;
        this.offset = 0;
        this.viewModelGetHistory(limit, offset);
        this.historyArrayList = new ArrayList<>();

        return view;
    }

    private void viewModelGetHistory(int limit, int offset) {
        travelindoViewModel = new ViewModelProvider(this).get(TravelindoViewModel.class);
        travelindoViewModel.getHistory(this.token, limit, offset);
        travelindoViewModel.getPayloadHistoryMutableLiveData().observe(getViewLifecycleOwner(), new Observer<PayloadHistory>() {
            @Override
            public void onChanged(PayloadHistory payloadHistory) {
                if (payloadHistory == null) {
                    return;
                } else {
                    for (int i = 0; i < payloadHistory.getHistories().size(); i++) {
                        historyArrayList.add(payloadHistory.getHistories().get(i));
                    }

                    ListView listView = binding.lvHistori;
                    HistoryAdapter historyAdapter = new HistoryAdapter(getActivity(), historyArrayList);
                    listView.setAdapter(historyAdapter);
                }
            }
        });
    }
}
