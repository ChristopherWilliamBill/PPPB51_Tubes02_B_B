package com.pppb.travelindo.View;

import androidx.annotation.NonNull;
import androidx.appcompat.app.ActionBarDrawerToggle;
import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentResultListener;
import androidx.fragment.app.FragmentTransaction;

import android.os.Bundle;

import com.pppb.travelindo.Model.User;
import com.pppb.travelindo.R;
import com.pppb.travelindo.databinding.ActivityHomeBinding;

import org.parceler.Parcels;

public class HomeActivity extends AppCompatActivity {
    private ActivityHomeBinding binding;
    private LoginFragment loginFragment;
    private HomeFragment homeFragment;
    private OrderFragment orderFragment;
    private HistoryFragment historyFragment;
    private TempatDudukBesar tempatDudukBesar;
    private TempatDudukKecil tempatDudukKecil;
    private PembayaranFragment pembayaranFragment;
    private TerimakasihFragment terimakasihFragment;

    private FragmentManager fragmentManager;
    private String token;
    private String username;
    private String course_id;
    private int[] seats;
    private int harga;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        this.binding = ActivityHomeBinding.inflate(getLayoutInflater());
        setContentView(this.binding.getRoot());

        this.setSupportActionBar(this.binding.toolbar);
        ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, this.binding.drawerLayout, this.binding.toolbar, R.string.openDrawer, R.string.closeDrawer);
        actionBarDrawerToggle.syncState();

        this.loginFragment = LoginFragment.newInstance();

        this.fragmentManager = this.getSupportFragmentManager();
        FragmentTransaction fragmentTransaction = this.fragmentManager.beginTransaction();
        fragmentTransaction.add(this.binding.fragmentContainer.getId(), this.loginFragment).addToBackStack(null);
        fragmentTransaction.commit();

        this.getSupportFragmentManager().setFragmentResultListener("changePage", this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
                int page = result.getInt("page");
                changePage(page);
            }
        });

        this.getSupportFragmentManager().setFragmentResultListener("cariTempatDuduk", this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
                int page = result.getInt("page");
                course_id = result.getString("course_id");
                seats = result.getIntArray("seats");
                harga = result.getInt("price");
                changePage(page);
            }
        });

        this.getSupportFragmentManager().setFragmentResultListener("user", this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
                User user = (User) Parcels.unwrap(result.getParcelable("user"));
                token = user.getToken();
                username = user.getUsername();
            }
        });
    }

    private void changePage(int page){
        FragmentTransaction ft = this.fragmentManager.beginTransaction();
        if(page == 1){ //fragment home
            this.homeFragment = HomeFragment.newInstance(this.token, this.username);
            ft.replace(this.binding.fragmentContainer.getId(), homeFragment).addToBackStack(null);
        }else if(page == 2){ //fragment order
            this.orderFragment = OrderFragment.newInstance(this.token);
            ft.replace(this.binding.fragmentContainer.getId(), orderFragment).addToBackStack(null);
        }else if(page == 3) { //fragment history
            this.historyFragment = HistoryFragment.newInstance(this.token);
            ft.replace(this.binding.fragmentContainer.getId(), historyFragment).addToBackStack(null);
        }else if(page == 4) { //fragment tempat duduk besar
            this.tempatDudukBesar = TempatDudukBesar.newInstance(this.token, this.course_id, this.seats);
            ft.replace(this.binding.fragmentContainer.getId(), tempatDudukBesar).addToBackStack(null);
        }else if(page == 5) { //fragment tempat duduk kecil
            this.tempatDudukKecil = TempatDudukKecil.newInstance(this.token, this.course_id, this.seats);
            ft.replace(this.binding.fragmentContainer.getId(), tempatDudukKecil).addToBackStack(null);
        }else if(page == 6) { //fragment Pembayaran
            this.pembayaranFragment = PembayaranFragment.newInstance(this.token, this.course_id, this.harga);
            ft.replace(binding.fragmentContainer.getId(), pembayaranFragment).addToBackStack(null);
        }else if(page == 7){//close app
            finish();
            System.exit(0);
        }else if(page == 8){
            this.terimakasihFragment = TerimakasihFragment.newInstance(this.harga, this.username);
            ft.replace(binding.fragmentContainer.getId(), terimakasihFragment).addToBackStack(null);
        }
        this.binding.drawerLayout.closeDrawers();
        ft.commit();
    }
}