package com.pppb.travelindo.View;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.pppb.travelindo.Network.TravelAPI;
import com.pppb.travelindo.databinding.HomeFragmentBinding;
import com.vstechlab.easyfonts.EasyFonts;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class HomeFragment extends Fragment implements View.OnClickListener{

    private HomeFragmentBinding binding;

    public HomeFragment(){ }

    public static HomeFragment newInstance(String token, String username){
        HomeFragment homeFragment = new HomeFragment();
        Bundle args = new Bundle();
        args.putString("token", token);
        args.putString("username", username);

        homeFragment.setArguments(args);
        return homeFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        this.binding = HomeFragmentBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        this.binding.tvWelcome.setText("Welcome, " + this.getArguments().getString("username"));
        this.binding.btnPesan.setOnClickListener(this);
        this.binding.tvWelcome.setTypeface(EasyFonts.robotoBold(this.getContext()));
        this.binding.btnPesan.setTypeface(EasyFonts.caviarDreamsBold(this.getContext()));
        return view;
    }

    @Override
    public void onClick(View view) {
        if(view == this.binding.btnPesan){
            FragmentManager fragmentManager = this.getParentFragmentManager();
            Bundle result = new Bundle();
            result.putInt("page", 2);
            fragmentManager.setFragmentResult("changePage", result);
        }
    }
}
