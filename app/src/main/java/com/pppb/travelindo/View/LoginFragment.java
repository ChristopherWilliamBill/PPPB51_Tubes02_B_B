package com.pppb.travelindo.View;

import android.content.Context;
import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.inputmethod.InputMethodManager;

import androidx.appcompat.app.AppCompatActivity;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.pppb.travelindo.Model.User;
import com.pppb.travelindo.Network.TravelAPI;
import com.pppb.travelindo.ViewModel.TravelindoViewModel;
import com.pppb.travelindo.databinding.LoginFragmentBinding;
import com.vstechlab.easyfonts.EasyFonts;

import org.parceler.Parcels;

public class LoginFragment extends Fragment implements View.OnClickListener{
    private LoginFragmentBinding binding;
    private String token;
    private Context context;
    private TravelindoViewModel travelindoViewModel;

    public LoginFragment(){ }

    public static LoginFragment newInstance(){
        LoginFragment loginFragment = new LoginFragment();
        Bundle args = new Bundle();
        loginFragment.setArguments(args);

        return loginFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        this.binding = LoginFragmentBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        this.binding.btnLogin.setOnClickListener(this);
        this.context = this.getContext();
        this.binding.tvLogIn.setTypeface(EasyFonts.caviarDreamsBold(this.context));
        this.binding.etUsername.setTypeface(EasyFonts.droidSerifRegular(this.context));
        this.binding.etPassword.setTypeface(EasyFonts.droidSerifRegular(this.context));
        this.binding.btnLogin.setTypeface(EasyFonts.caviarDreamsBold(this.context));
        this.viewModelLogin();
        return view;
    }

    @Override
    public void onResume() {
        super.onResume();
        ((AppCompatActivity)getActivity()).getSupportActionBar().hide();
    }

    @Override
    public void onStop() {
        super.onStop();
        ((AppCompatActivity)getActivity()).getSupportActionBar().show();
    }

    private void setToken(String token){
        this.token = token;
    }

    private void viewModelLogin(){
        travelindoViewModel = new ViewModelProvider(this).get(TravelindoViewModel.class);
        travelindoViewModel.getUserMutableLiveData().observe(getViewLifecycleOwner(), new Observer<User>() {
            @Override
            public void onChanged(User user) {
                if(user == null){
                    binding.passSalah.setText("Login gagal, Password/Username tidak sesuai");
                }else{
                    user.setUsername(binding.etUsername.getText().toString());
                    setToken(user.getToken());
                    FragmentManager fragmentManager = getParentFragmentManager();
                    Bundle result = new Bundle();
                    result.putParcelable("user", Parcels.wrap(user));
                    fragmentManager.setFragmentResult("user", result);

                    Bundle result2 = new Bundle();
                    result2.putInt("page", 1);
                    fragmentManager.setFragmentResult("changePage", result2);
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        if(view == this.binding.btnLogin){
            if(!this.binding.etPassword.getText().toString().equals("") && !this.binding.etUsername.getText().toString().equals("")) {
                String username = this.binding.etUsername.getText().toString();
                String password = this.binding.etPassword.getText().toString();
                travelindoViewModel.logIn(username, password);
                InputMethodManager imm = (InputMethodManager) context.getSystemService(context.INPUT_METHOD_SERVICE);
                imm.hideSoftInputFromWindow(view.getWindowToken(), 0);
            }
        }
    }
}
