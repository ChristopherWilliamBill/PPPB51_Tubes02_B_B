package com.pppb.travelindo.View;

import android.os.Bundle;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.pppb.travelindo.Network.TravelAPI;
import com.pppb.travelindo.databinding.NavigationFragmentBinding;
import com.vstechlab.easyfonts.EasyFonts;

import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class NavigationFragment extends Fragment implements View.OnClickListener{
    private NavigationFragmentBinding binding;
    private HomeFragment homeFragment;
    private HistoryFragment historyFragment;
    private FragmentManager fragmentManager;
    private TravelAPI travelAPI;
    private String token;
    private String username;

    public NavigationFragment(){ }

    public static NavigationFragment newInstance(String token, String username){
        NavigationFragment navigationFragment = new NavigationFragment();
        Bundle args = new Bundle();
        args.putString("token", token);
        args.putString("username", username);

        navigationFragment.setArguments(args);
        return navigationFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        this.binding = NavigationFragmentBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        binding.btnHome.setOnClickListener(this);
        binding.btnHistory.setOnClickListener(this);
        binding.btnExit.setOnClickListener(this);

        this.binding.btnHome.setTypeface(EasyFonts.caviarDreamsBold(this.getContext()));
        this.binding.btnHistory.setTypeface(EasyFonts.caviarDreamsBold(this.getContext()));
        this.binding.btnExit.setTypeface(EasyFonts.caviarDreamsBold(this.getContext()));
        return view;
    }

    @Override
    public void onClick(View view) {
        FragmentManager fragmentManager = this.getParentFragmentManager();
        Bundle result = new Bundle();
        if(view == binding.btnHome){
            result.putInt("page",1);
            fragmentManager.setFragmentResult("changePage", result);
        }
        else if(view == binding.btnHistory){
            result.putInt("page",3);
            fragmentManager.setFragmentResult("changePage", result);
        }
        else if(view == binding.btnExit){
            result.putInt("page",7);
            fragmentManager.setFragmentResult("changePage", result);
        }
    }
}

