package com.pppb.travelindo.View;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ArrayAdapter;
import android.widget.RadioGroup;
import android.widget.TextView;
import android.widget.Toast;

import androidx.fragment.app.DialogFragment;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.appeaser.sublimepickerlibrary.datepicker.SelectedDate;
import com.appeaser.sublimepickerlibrary.helpers.SublimeOptions;
import com.appeaser.sublimepickerlibrary.recurrencepicker.SublimeRecurrencePicker;
import com.pppb.travelindo.Model.Course;
import com.pppb.travelindo.Model.PayloadCourses;
import com.pppb.travelindo.Model.PayloadRoutes;
import com.pppb.travelindo.Model.User;
import com.pppb.travelindo.R;
import com.pppb.travelindo.Network.TravelAPI;
import com.pppb.travelindo.ViewModel.TravelindoViewModel;
import com.pppb.travelindo.databinding.OrderPageBinding;
import com.vstechlab.easyfonts.EasyFonts;

import org.parceler.Parcels;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class OrderFragment extends Fragment implements View.OnClickListener {
    private OrderPageBinding binding;
    private String token;
    private static final String TAG = "OrderFragment";
    private Context context;
    private TravelindoViewModel travelindoViewModel;

    private String ukuranMobil;

    public OrderFragment(){ }

    public static OrderFragment newInstance(String token){
        OrderFragment orderFragment = new OrderFragment();
        Bundle args = new Bundle();
        args.putString("Token", token);
        orderFragment.setArguments(args);

        return orderFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        this.binding = OrderPageBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        this.token = this.getArguments().getString("Token");
        this.binding.btnCari.setOnClickListener(this);
        this.binding.pilihTanggal.setOnClickListener(this);
        this.binding.pilihJam.setOnClickListener(this);
        this.binding.idBackOrderBTN.setOnClickListener(this);
        this.context = this.getContext();
        changeAllFont();
        this.viewModelGetRoutes();

        RadioGroup radioGroup = this.binding.radioGroup;
        radioGroup.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup radioGroup, int i) {
                switch(i){
                    case R.id.id_radioBesar:
                        ukuranMobil = "Large";
                        break;
                    case R.id.id_radioKecil:
                        ukuranMobil = "Small";
                        break;
                }
            }
        });

        return view;
    }

    private void viewModelGetRoutes(){
        travelindoViewModel = new ViewModelProvider(this).get(TravelindoViewModel.class);
        travelindoViewModel.getRoutes(token);
        travelindoViewModel.getRoutesMutableLiveData().observe(getViewLifecycleOwner(), new Observer<PayloadRoutes>() {
            @Override
            public void onChanged(PayloadRoutes payloadRoutes) {
                if(payloadRoutes == null){
                    return;
                }else{
                    List<String> listKotaAsal = new ArrayList<>();
                    List<String> listKotaTujuan = new ArrayList<>();
                    for(int i = 0; i < payloadRoutes.getRoutes().size(); i++){
                        Log.d("Source", payloadRoutes.getRoutes().get(i).getSource());
                        Log.d("Destination", payloadRoutes.getRoutes().get(i).getDestination());

                        if(!listKotaAsal.contains(payloadRoutes.getRoutes().get(i).getSource())){
                            listKotaAsal.add(payloadRoutes.getRoutes().get(i).getSource());
                        }

                        if(!listKotaTujuan.contains(payloadRoutes.getRoutes().get(i).getDestination())){
                            listKotaTujuan.add(payloadRoutes.getRoutes().get(i).getDestination());
                        }
                    }

                    ArrayAdapter<String> kotaAsalAdapter = new ArrayAdapter(context, android.R.layout.simple_list_item_1, listKotaAsal);
                    ArrayAdapter<String> kotaTujuanAdapter = new ArrayAdapter(context, android.R.layout.simple_list_item_1, listKotaTujuan);
                    binding.dropdownKotaAsal.setAdapter(kotaAsalAdapter);
                    binding.dropdownKotaTujuan.setAdapter(kotaTujuanAdapter);
                    kotaTujuanAdapter.notifyDataSetChanged();
                    kotaAsalAdapter.notifyDataSetChanged();
                }
            }
        });
    }

    private void viewModelGetCourses(){
        travelindoViewModel = new ViewModelProvider(this).get(TravelindoViewModel.class);
        travelindoViewModel.getCourse(
                this.token,
                this.binding.dropdownKotaAsal.getSelectedItem().toString(),
                this.binding.dropdownKotaTujuan.getSelectedItem().toString(),
                this.ukuranMobil,
                this.binding.tvTanggal.getText().toString(),
                this.binding.tvJam.getText().toString()
        );
        travelindoViewModel.getCourseMutableLiveData().observe(getViewLifecycleOwner(), new Observer<PayloadCourses>() {
            @Override
            public void onChanged(PayloadCourses payloadCourses) {
                if(payloadCourses == null){
                    Toast toast = Toast.makeText(context, "Travel tidak tersedia pada jam/tanggal tersebut", Toast.LENGTH_SHORT);
                    toast.show();
                    return;
                }else{
                    FragmentManager fragmentManager = getParentFragmentManager();
                    Bundle result = new Bundle();
                    if(ukuranMobil.equals("Large")){
                        result.putInt("page", 4);
                        result.putIntArray("seats", payloadCourses.getCourses().get(0).getSeats());

                    }else if(ukuranMobil.equals("Small")){
                        result.putInt("page", 5);
                        result.putIntArray("seats", payloadCourses.getCourses().get(0).getSeats());
                    }
                    result.putString("course_id", payloadCourses.getCourses().get(0).getCourse_id());
                    result.putInt("price", payloadCourses.getCourses().get(0).getFee());
                    fragmentManager.setFragmentResult("cariTempatDuduk", result);

                    //kirim ke pembayaran
                    Bundle result2 = new Bundle();
                    Course course = payloadCourses.getCourses().get(0);
                    result2.putParcelable("course", Parcels.wrap(course));
                    result2.putString("ukuranMobil", ukuranMobil);
                    result2.putString("tanggal", binding.tvTanggal.getText().toString());
                    result2.putString("jam", binding.tvJam.getText().toString());

                    fragmentManager.setFragmentResult("pembayaran2", result2);
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        if(view == this.binding.pilihTanggal){
            sublimePicker();
        }
        else if(view == this.binding.pilihJam){
            sublimePicker();
        }
        else if(view == this.binding.btnCari){
            viewModelGetCourses();
        }
        else if(view == this.binding.idBackOrderBTN){
            FragmentManager fragmentManager = this.getParentFragmentManager();
            Bundle result = new Bundle();
            result.putInt("page", 1);
            fragmentManager.setFragmentResult("changePage", result);
        }
    }

    public void sublimePicker() {
        DateTimePickerFragment.Callback mFragmentCallback = new DateTimePickerFragment.Callback() {
            @Override
            public void onCancelled() {
            }

            @Override
            public void onDateTimeReccurenceSet(SelectedDate selectedDate, int hourOfDay, int minute, SublimeRecurrencePicker.RecurrenceOption recurrenceOption, String recurrenceRule) {
                if (selectedDate != null) {
                    int year = selectedDate.getEndDate().get(Calendar.YEAR);
                    int month = selectedDate.getEndDate().get(Calendar.MONTH) + 1;
                    int dayOfMonth = selectedDate.getEndDate().get(Calendar.DATE);

                    Calendar calendar = Calendar.getInstance();
                    calendar.set(Calendar.YEAR, year);
                    calendar.set(Calendar.MONTH, month);
                    calendar.set(Calendar.DATE, dayOfMonth);
                    String tanggalPilihan = dayOfMonth + "-" + month + "-" + year;
                    String jamPilihan = hourOfDay + "";

                    binding.tvTanggal.setText(tanggalPilihan);
                    binding.tvJam.setText(jamPilihan);
                }
            }

            @Override
            public void onOkay() {
            }
        };
        DateTimePickerFragment datePicker = new DateTimePickerFragment();
        datePicker.setCallback(mFragmentCallback);

        SublimeOptions opts = new SublimeOptions().setCanPickDateRange(false).setDisplayOptions(SublimeOptions.ACTIVATE_DATE_PICKER | SublimeOptions.ACTIVATE_TIME_PICKER).setPickerToShow(SublimeOptions.Picker.TIME_PICKER);
        Bundle bundle = new Bundle();
        bundle.putParcelable("SUBLIME_OPTIONS", opts);
        datePicker.setArguments(bundle);
        datePicker.setStyle(DialogFragment.STYLE_NO_TITLE, 0);
        datePicker.show(getActivity().getSupportFragmentManager(), "SUBLIME_PICKER");
    }

    public void changeAllFont(){
        //title
        this.binding.titleOrder.setTypeface(EasyFonts.caviarDreamsBold(this.getContext()));
        this.binding.titleAsal.setTypeface(EasyFonts.caviarDreamsBold(this.getContext()));
        this.binding.titleTujuan.setTypeface(EasyFonts.caviarDreamsBold(this.getContext()));
        this.binding.titleTgl.setTypeface(EasyFonts.caviarDreamsBold(this.getContext()));
        this.binding.titleJam.setTypeface(EasyFonts.caviarDreamsBold(this.getContext()));
        this.binding.titleCar.setTypeface(EasyFonts.caviarDreamsBold(this.getContext()));

        this.binding.tvTanggal.setTypeface(EasyFonts.robotoRegular(this.getContext()));
        this.binding.tvJam.setTypeface(EasyFonts.robotoRegular(this.getContext()));

        //button
        this.binding.idBackOrderBTN.setTypeface(EasyFonts.caviarDreamsBold(this.context));
        this.binding.btnCari.setTypeface(EasyFonts.caviarDreamsBold(this.context));
    }
}
