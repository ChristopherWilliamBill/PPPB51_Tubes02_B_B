package com.pppb.travelindo.View;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentResultListener;
import androidx.lifecycle.Observer;
import androidx.lifecycle.ViewModelProvider;

import com.pppb.travelindo.Model.Course;
import com.pppb.travelindo.Model.HasilOrder;
import com.pppb.travelindo.Model.Order;
import com.pppb.travelindo.Model.PayloadCourses;
import com.pppb.travelindo.Network.TravelAPI;
import com.pppb.travelindo.ViewModel.TravelindoViewModel;
import com.pppb.travelindo.databinding.PembayaranPageBinding;
import com.vstechlab.easyfonts.EasyFonts;

import org.parceler.Parcels;

import okhttp3.OkHttpClient;
import okhttp3.logging.HttpLoggingInterceptor;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import retrofit2.Retrofit;
import retrofit2.converter.gson.GsonConverterFactory;

public class PembayaranFragment extends Fragment implements View.OnClickListener{

    private PembayaranPageBinding binding;
    private String token;
    private String course_id;
    private String seats;
    private Context context;
    private String ukuranMobil;
    private int harga;
    private int jumlah;
    private Course course;
    private HasilOrder hasilOrder;

    private String asal;
    private String tujuan;
    private String tanggal;
    private String jam;

    private TravelindoViewModel travelindoViewModel;

    public PembayaranFragment(){ }

    public static PembayaranFragment newInstance(String token, String course_id, int harga){
        PembayaranFragment pembayaranFragment = new PembayaranFragment();
        Bundle args = new Bundle();
        args.putString("Token",token);
        args.putString("CourseId", course_id);
        args.putInt("price", harga);
        pembayaranFragment.setArguments(args);
        return pembayaranFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        this.binding = PembayaranPageBinding.inflate(inflater, container, false);
        View view = binding.getRoot();
        this.token = this.getArguments().getString("Token");
        this.course_id = this.getArguments().getString("CourseId");
        this.seats = this.getArguments().getString("kursiDipilih");
        this.harga = this.getArguments().getInt("price");

        this.binding.idBackPembayaranBTN.setOnClickListener(this);
        this.binding.btnBayar.setOnClickListener(this);
        this.context = this.getContext();
        changeAllFont();
        this.setText();
        return view;
    }

    private void setText(){
        this.getParentFragmentManager().setFragmentResultListener("pembayaran2", this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
                course = (Course) Parcels.unwrap(result.getParcelable("course"));
                asal = course.getSource();
                tujuan = course.getDestination();
                ukuranMobil = course.getVehicle();

                tanggal = result.getString("tanggal");
                jam = result.getString("jam");

                binding.kotaAsal.setText(asal);
                binding.kotaTujuan.setText(tujuan);
                binding.tanggal.setText(tanggal);
                binding.jam.setText(jam);
            }
        });

        this.getParentFragmentManager().setFragmentResultListener("pembayaran1", this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
                seats = result.getString("kursiDipilih");
                if(ukuranMobil.equals("Small")){
                    ukuranMobil = "Kecil";
                    jumlah = (result.getInt("jumlahKursi")) * harga;
                }else{
                    ukuranMobil = "Besar";
                    jumlah = (result.getInt("jumlahKursi")) * harga;
                }

                binding.totalKursi.setText(result.getInt("jumlahKursi")+"");
                binding.hargaSatuan.setText("Rp. " + harga);
                binding.nomorKursi.setText(seats);
                binding.ukuranMobil.setText(ukuranMobil);
                binding.total.setText("Rp. " + jumlah);
            }
        });
    }

    private void viewModelAddNewOrder() {
        travelindoViewModel = new ViewModelProvider(this).get(TravelindoViewModel.class);
        travelindoViewModel.addNewOrder(this.token, this.course_id, this.seats);
        travelindoViewModel.getOrderMutableLiveData().observe(getViewLifecycleOwner(), new Observer<Order>() {
            @Override
            public void onChanged(Order order) {
                if (order == null) {
                    Toast toast = Toast.makeText(context, "Terdapat error", Toast.LENGTH_SHORT);
                    toast.show();
                    return;
                } else {
                    hasilOrder = order.getPayload().get(0);
                    Toast toast = Toast.makeText(context, order.getMessage(), Toast.LENGTH_SHORT);
                    toast.show();
                    keTerimaKasih();
                    FragmentManager fragmentManager = getParentFragmentManager();
                    Bundle result = new Bundle();
                    result.putInt("page", 8);
                    fragmentManager.setFragmentResult("changePage", result);
                }
            }
        });
    }

    @Override
    public void onClick(View view) {
        if(view == this.binding.idBackPembayaranBTN){
            FragmentManager fragmentManager = this.getParentFragmentManager();
            Bundle result = new Bundle();
            result.putInt("page", 2);
            fragmentManager.setFragmentResult("changePage", result);
        }else if(view == this.binding.btnBayar){
            this.viewModelAddNewOrder();
        }
    }

    private void keTerimaKasih(){
        FragmentManager fragmentManager = this.getParentFragmentManager();
        Bundle result = new Bundle();
        result.putParcelable("course", Parcels.wrap(course));
        result.putParcelable("hasilOrder", Parcels.wrap(hasilOrder));

        result.putString("asal", asal);
        result.putString("tujuan", tujuan);
        result.putString("nomorKursi", seats);
        result.putInt("harga", harga);
        result.putString("tanggal", tanggal);
        result.putString("jam", jam);
        result.putInt("jumlah", jumlah);

        fragmentManager.setFragmentResult("kirimKeTerimakasih", result);
    }
    public void changeAllFont(){
        //title
        this.binding.titlePembayaran.setTypeface(EasyFonts.caviarDreamsBold(this.getContext()));
        this.binding.titleAsal.setTypeface(EasyFonts.caviarDreamsBold(this.getContext()));
        this.binding.titleTujuan.setTypeface(EasyFonts.caviarDreamsBold(this.getContext()));
        this.binding.titleTgl.setTypeface(EasyFonts.caviarDreamsBold(this.getContext()));
        this.binding.titleJam.setTypeface(EasyFonts.caviarDreamsBold(this.getContext()));
        this.binding.titleKursi.setTypeface(EasyFonts.caviarDreamsBold(this.getContext()));
        this.binding.titleJmlKursi.setTypeface(EasyFonts.caviarDreamsBold(this.getContext()));
        this.binding.titleHargaSatuan.setTypeface(EasyFonts.caviarDreamsBold(this.getContext()));
        this.binding.titleTotal.setTypeface(EasyFonts.caviarDreamsBold(this.getContext()));
        this.binding.titleUkuran.setTypeface(EasyFonts.caviarDreamsBold(this.getContext()));

        //isi
        this.binding.kotaAsal.setTypeface(EasyFonts.robotoRegular(this.getContext()));
        this.binding.kotaTujuan.setTypeface(EasyFonts.robotoRegular(this.getContext()));
        this.binding.tanggal.setTypeface(EasyFonts.robotoRegular(this.getContext()));
        this.binding.jam.setTypeface(EasyFonts.robotoRegular(this.getContext()));
        this.binding.nomorKursi.setTypeface(EasyFonts.robotoRegular(this.getContext()));
        this.binding.totalKursi.setTypeface(EasyFonts.robotoRegular(this.getContext()));
        this.binding.hargaSatuan.setTypeface(EasyFonts.robotoRegular(this.getContext()));
        this.binding.total.setTypeface(EasyFonts.robotoRegular(this.getContext()));
        this.binding.ukuranMobil.setTypeface(EasyFonts.robotoRegular(this.getContext()));

        //button
        this.binding.idBackPembayaranBTN.setTypeface(EasyFonts.caviarDreamsBold(this.context));
        this.binding.btnBayar.setTypeface(EasyFonts.caviarDreamsBold(this.context));
    }
}