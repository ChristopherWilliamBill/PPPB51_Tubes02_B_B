package com.pppb.travelindo.View;

import android.graphics.Bitmap;
import android.graphics.Canvas;
import android.graphics.Color;
import android.graphics.Paint;
import android.graphics.RectF;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import androidx.core.content.res.ResourcesCompat;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;

import com.pppb.travelindo.R;
import com.pppb.travelindo.databinding.FragmentTempatDudukKecilBinding;
import com.vstechlab.easyfonts.EasyFonts;

import java.util.LinkedList;

public class TempatDudukKecil extends Fragment implements View.OnClickListener, View.OnTouchListener{
    private FragmentTempatDudukKecilBinding binding;
    private Canvas mCanvas;
    private int x2 = 0;
    private int y2 = 0;
    private int arr[] = {0,0,0,0,0,0};
    private int arrDipilih[] = {0,0,0,0,0,0};
    private int mColorBackground;
    private int mColorKosong;
    private int mColorTerisi;
    private int mColorDipilih;
    private Paint paintKosong = new Paint();
    private Paint paintTerisi = new Paint();
    private Paint paintDipilih = new Paint();
    private Paint strokePaint = new Paint();
    private Paint paintNumber = new Paint();
    private LinkedList<Integer> ll;
    private String kursiDipilih;

    private int arrMasukan[];

    public TempatDudukKecil() {
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        this.binding = FragmentTempatDudukKecilBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        int x = 974;
        int y = 700;
        Bitmap mBitmap = Bitmap.createBitmap(x,y, Bitmap.Config.ARGB_8888);
        binding.ivCanvasKecil.setImageBitmap(mBitmap);
        mCanvas = new Canvas(mBitmap);

        arrMasukan = this.getArguments().getIntArray("seats");
        match(arrMasukan);
        setBackground();
        setWarnaKursi();

        binding.ivCanvasKecil.setOnTouchListener(this);
        binding.idBackKursiBTN.setOnClickListener(this);
        binding.idPesanKursiBTN.setOnClickListener(this);
        changeAllFont();

        this.ll = new LinkedList<>();
        kursiDipilih = "";

        return view;
    }

    public static TempatDudukKecil newInstance(String token, String course_id, int[] seats) {
        TempatDudukKecil tempatDudukKecil = new TempatDudukKecil();
        Bundle args = new Bundle();
        args.putString("token", token);
        args.putIntArray("seats", seats);
        args.putString("course_id", course_id);
        tempatDudukKecil.setArguments(args);
        return tempatDudukKecil;
    }

    @Override
    public void onClick(View v) {
        if(binding.idBackKursiBTN == v){
            FragmentManager fragmentManager = this.getParentFragmentManager();
            Bundle result = new Bundle();
            result.putInt("page", 2);
            fragmentManager.setFragmentResult("changePage", result);
        }else if(binding.idPesanKursiBTN == v){
            match2(arrDipilih);
            if(kursiDipilih.length() == 0){
                Toast.makeText(getActivity(), "Kursi Belum Dipilih!", Toast.LENGTH_SHORT).show();
            }else {
                FragmentManager fragmentManager = this.getParentFragmentManager();
                Bundle result = new Bundle();
                result.putInt("page", 6);
                fragmentManager.setFragmentResult("changePage", result);

                //kirim ke pembayaran
                Bundle result2 = new Bundle();
                result2.putString("kursiDipilih", kursiDipilih);
                result2.putInt("jumlahKursi", ll.size());
                fragmentManager.setFragmentResult("pembayaran1", result2);
            }
        }
    }

    private void match(int arrMasukan[]){
        for (int i = 0; i < arrMasukan.length; i++){
            arr[arrMasukan[i] - 1] = 1;
        }
    }

    private void match2(int arrDipilih[]){
        for (int i = 0; i < arrDipilih.length; i++){
            if(arrDipilih[i] == 1){
                ll.add(i+1);
            }
        }
        for (int i = 0; i < ll.size(); i++){
            if (i == ll.size()-1){
                this.kursiDipilih = this.kursiDipilih + (ll.get(i));
            }else{
                this.kursiDipilih = this.kursiDipilih + (ll.get(i) + ",");
            }
        }
    }

    @Override
    public boolean onTouch(View v, MotionEvent motionEvent) {
        int x = (int)motionEvent.getX();
        int y = (int)motionEvent.getY();
        switch(motionEvent.getAction() & motionEvent.ACTION_MASK){
            case MotionEvent.ACTION_DOWN:
                if(x >= 224 && x <= 324 && y >= 100 && y <= 200){
                    if(arr[0] == 0){
                        if(arrDipilih[0] == 0){
                            mCanvas.drawRoundRect(new RectF(224, 100, 324, 200), 20, 20, paintDipilih);
                            mCanvas.drawRoundRect(new RectF(224, 100, 324, 200), 20, 20, strokePaint);
                            mCanvas.drawText("1", 261, 165, paintNumber);
                            arrDipilih[0] = 1;
                        }else{
                            mCanvas.drawRoundRect(new RectF(224, 100, 324, 200), 20, 20, paintKosong);
                            mCanvas.drawRoundRect(new RectF(224, 100, 324, 200), 20, 20, strokePaint);
                            mCanvas.drawText("1", 261, 165, paintNumber);
                            arrDipilih[0] = 0;
                        }
                    }else{
                        Toast.makeText(getActivity(), "Kursi 1 sudah terisi!", Toast.LENGTH_SHORT).show();
                    }
                }else if(x >= 669 && x <= 769 && y >= 100 && y <= 200){
                    if(arr[1] == 0){
                        if(arrDipilih[1] == 0){
                            mCanvas.drawRoundRect(new RectF(669, 100, 769, 200), 20, 20, paintDipilih);
                            mCanvas.drawRoundRect(new RectF(669, 100, 769, 200), 20, 20, strokePaint);
                            mCanvas.drawText("2", 706, 165, paintNumber);
                            arrDipilih[1] = 1;
                        }else{
                            mCanvas.drawRoundRect(new RectF(669, 100, 769, 200), 20, 20, paintKosong);
                            mCanvas.drawRoundRect(new RectF(669, 100, 769, 200), 20, 20, strokePaint);
                            mCanvas.drawText("2", 706, 165, paintNumber);
                            arrDipilih[1] = 0;
                        }

                    }else{
                        Toast.makeText(getActivity(), "Kursi 2 sudah terisi!", Toast.LENGTH_SHORT).show();
                    }
                }else if(x >= 224 && x <= 324 && y >= 275 && y <= 375){
                    if(arr[2] == 0){
                        if(arrDipilih[2] == 0){
                            mCanvas.drawRoundRect(new RectF(224, 275, 324, 375), 20, 20, paintDipilih);
                            mCanvas.drawRoundRect(new RectF(224, 275, 324, 375), 20, 20, strokePaint);
                            mCanvas.drawText("3", 261, 340, paintNumber);
                            arrDipilih[2] = 1;
                        }else{
                            mCanvas.drawRoundRect(new RectF(224, 275, 324, 375), 20, 20, paintKosong);
                            mCanvas.drawRoundRect(new RectF(224, 275, 324, 375), 20, 20, strokePaint);
                            mCanvas.drawText("3", 261, 340, paintNumber);
                            arrDipilih[2] = 0;
                        }
                    }else{
                        Toast.makeText(getActivity(), "Kursi 3 sudah terisi!", Toast.LENGTH_SHORT).show();
                    }
                }else if(x >= 669 && x <= 769 && y >= 275 && y <= 375){
                    if(arr[3] == 0){
                        if(arrDipilih[3] == 0){
                            mCanvas.drawRoundRect(new RectF(669, 275, 769, 375), 20, 20, paintDipilih);
                            mCanvas.drawRoundRect(new RectF(669, 275, 769, 375), 20, 20, strokePaint);
                            mCanvas.drawText("4", 706, 340, paintNumber);
                            arrDipilih[3] = 1;
                        }else{
                            mCanvas.drawRoundRect(new RectF(669, 275, 769, 375), 20, 20, paintKosong);
                            mCanvas.drawRoundRect(new RectF(669, 275, 769, 375), 20, 20, strokePaint);
                            mCanvas.drawText("4", 706, 340, paintNumber);
                            arrDipilih[3] = 0;
                        }
                    }else{
                        Toast.makeText(getActivity(), "Kursi 4 sudah terisi!", Toast.LENGTH_SHORT).show();
                    }
                }else if(x >= 224 && x <= 324 && y >= 450 && y <= 550){
                    if(arr[4] == 0){
                        if(arrDipilih[4] == 0){
                            mCanvas.drawRoundRect(new RectF(224, 450, 324, 550), 20, 20, paintDipilih);
                            mCanvas.drawRoundRect(new RectF(224, 450, 324, 550), 20, 20, strokePaint);
                            mCanvas.drawText("5", 261, 515, paintNumber);
                            arrDipilih[4] = 1;
                        }else{
                            mCanvas.drawRoundRect(new RectF(224, 450, 324, 550), 20, 20, paintKosong);
                            mCanvas.drawRoundRect(new RectF(224, 450, 324, 550), 20, 20, strokePaint);
                            mCanvas.drawText("5", 261, 515, paintNumber);
                            arrDipilih[4] = 0;
                        }
                    }else{
                        Toast.makeText(getActivity(), "Kursi 5 sudah terisi!", Toast.LENGTH_SHORT).show();
                    }
                }else if(x >= 669 && x <= 769 && y >= 450 && y <= 550){
                    if(arr[5] == 0){
                        if(arrDipilih[5] == 0){
                            mCanvas.drawRoundRect(new RectF(669, 450, 769, 550), 20, 20, paintDipilih);
                            mCanvas.drawRoundRect(new RectF(669, 450, 769, 550), 20, 20, strokePaint);
                            mCanvas.drawText("6", 706, 515, paintNumber);
                            arrDipilih[5] = 1;
                        }else{
                            mCanvas.drawRoundRect(new RectF(669, 450, 769, 550), 20, 20, paintKosong);
                            mCanvas.drawRoundRect(new RectF(669, 450, 769, 550), 20, 20, strokePaint);
                            mCanvas.drawText("6", 706, 515, paintNumber);
                            arrDipilih[5] = 0;
                        }
                    }else{
                        Toast.makeText(getActivity(), "Kursi 6 sudah terisi!", Toast.LENGTH_SHORT).show();
                    }
                }
                binding.ivCanvasKecil.invalidate();
                break;
        }
        return false;
    }

    private void setBackground(){
        mColorBackground = ResourcesCompat.getColor(getResources(), R.color.kosong, null);
        mCanvas.drawColor(mColorBackground);

        mColorKosong = ResourcesCompat.getColor(getResources(), R.color.kosong, null);
        paintKosong.setColor(mColorKosong);

        mColorTerisi = ResourcesCompat.getColor(getResources(), R.color.terisi, null);
        paintTerisi.setColor(mColorTerisi);

        mColorDipilih = ResourcesCompat.getColor(getResources(), R.color.dipilih, null);
        paintDipilih.setColor(mColorDipilih);

        strokePaint.setStyle(Paint.Style.STROKE);
        strokePaint.setColor(Color.BLACK);
        strokePaint.setStrokeWidth(5);

        paintNumber.setColor(Color.BLACK);
        paintNumber.setTextSize(40);
    }

    private void setWarnaKursi(){
        for (int i = 0; i < 6; i++){
            if(i == 0){
                if(arr[i] == 0){
                    mCanvas.drawRoundRect(new RectF(224, 100, 324, 200), 20, 20, paintKosong);
                    mCanvas.drawRoundRect(new RectF(224, 100, 324, 200), 20, 20, strokePaint);
                    mCanvas.drawText("1", 261, 165, paintNumber);
                }else{
                    mCanvas.drawRoundRect(new RectF(224, 100, 324, 200), 20, 20, paintTerisi);
                    mCanvas.drawRoundRect(new RectF(224, 100, 324, 200), 20, 20, strokePaint);
                    mCanvas.drawText("1", 261, 165, paintNumber);
                }
            }else if(i == 1){
                if(arr[i] == 0){
                    mCanvas.drawRoundRect(new RectF(669, 100, 769, 200), 20, 20, paintKosong);
                    mCanvas.drawRoundRect(new RectF(669, 100, 769, 200), 20, 20, strokePaint);
                    mCanvas.drawText("2", 706, 165, paintNumber);
                }else{
                    mCanvas.drawRoundRect(new RectF(669, 100, 769, 200), 20, 20, paintTerisi);
                    mCanvas.drawRoundRect(new RectF(669, 100, 769, 200), 20, 20, strokePaint);
                    mCanvas.drawText("2", 706, 165, paintNumber);
                }
            }else if(i == 2){
                if(arr[i] == 0){
                    mCanvas.drawRoundRect(new RectF(224, 275, 324, 375), 20, 20, paintKosong);
                    mCanvas.drawRoundRect(new RectF(224, 275, 324, 375), 20, 20, strokePaint);
                    mCanvas.drawText("3", 261, 340, paintNumber);

                }else{
                    mCanvas.drawRoundRect(new RectF(224, 275, 324, 375), 20, 20, paintTerisi);
                    mCanvas.drawRoundRect(new RectF(224, 275, 324, 375), 20, 20, strokePaint);
                    mCanvas.drawText("3", 261, 340, paintNumber);

                }
            }else if(i == 3){
                if(arr[i] == 0){
                    mCanvas.drawRoundRect(new RectF(669, 275, 769, 375), 20, 20, paintKosong);
                    mCanvas.drawRoundRect(new RectF(669, 275, 769, 375), 20, 20, strokePaint);
                    mCanvas.drawText("4", 706, 340, paintNumber);

                }else{
                    mCanvas.drawRoundRect(new RectF(669, 275, 769, 375), 20, 20, paintTerisi);
                    mCanvas.drawRoundRect(new RectF(669, 275, 769, 375), 20, 20, strokePaint);
                    mCanvas.drawText("4", 706, 340, paintNumber);

                }
            }else if(i == 4){
                if(arr[i] == 0){
                    mCanvas.drawRoundRect(new RectF(224, 450, 324, 550), 20, 20, paintKosong);
                    mCanvas.drawRoundRect(new RectF(224, 450, 324, 550), 20, 20, strokePaint);
                    mCanvas.drawText("5", 261, 515, paintNumber);

                }else{
                    mCanvas.drawRoundRect(new RectF(224, 450, 324, 550), 20, 20, paintTerisi);
                    mCanvas.drawRoundRect(new RectF(224, 450, 324, 550), 20, 20, strokePaint);
                    mCanvas.drawText("5", 261, 515, paintNumber);

                }
            }else if(i == 5){
                if(arr[i] == 0){
                    mCanvas.drawRoundRect(new RectF(669, 450, 769, 550), 20, 20, paintKosong);
                    mCanvas.drawRoundRect(new RectF(669, 450, 769, 550), 20, 20, strokePaint);
                    mCanvas.drawText("6", 706, 515, paintNumber);

                }else{
                    mCanvas.drawRoundRect(new RectF(669, 450, 769, 550), 20, 20, paintTerisi);
                    mCanvas.drawRoundRect(new RectF(669, 450, 769, 550), 20, 20, strokePaint);
                    mCanvas.drawText("6", 706, 515, paintNumber);

                }
            }
        }
        binding.ivCanvasKecil.invalidate();
    }
    public void changeAllFont(){
        //title
        this.binding.titlePilih.setTypeface(EasyFonts.caviarDreamsBold(this.getContext()));
        this.binding.titleKecil.setTypeface(EasyFonts.caviarDreamsBold(this.getContext()));

        //legend
        this.binding.terisi.setTypeface(EasyFonts.droidSerifRegular(this.getContext()));
        this.binding.dipilih.setTypeface(EasyFonts.droidSerifRegular(this.getContext()));
        this.binding.kosong.setTypeface(EasyFonts.droidSerifRegular(this.getContext()));

        //button
        this.binding.idBackKursiBTN.setTypeface(EasyFonts.caviarDreamsBold(this.getContext()));
        this.binding.idPesanKursiBTN.setTypeface(EasyFonts.caviarDreamsBold(this.getContext()));
    }
}