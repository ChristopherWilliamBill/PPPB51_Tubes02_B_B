package com.pppb.travelindo.View;

import android.content.Context;
import android.os.Bundle;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import androidx.annotation.NonNull;
import androidx.fragment.app.Fragment;
import androidx.fragment.app.FragmentManager;
import androidx.fragment.app.FragmentResultListener;

import com.bumptech.glide.Glide;
import com.bumptech.glide.load.engine.DiskCacheStrategy;
import com.pppb.travelindo.Model.Course;
import com.pppb.travelindo.Model.HasilOrder;
import com.pppb.travelindo.Network.TravelAPI;
import com.pppb.travelindo.databinding.FragmentTerimakasihBinding;
import com.vstechlab.easyfonts.EasyFonts;

import org.parceler.Parcels;

public class TerimakasihFragment extends Fragment implements View.OnClickListener{

    private FragmentTerimakasihBinding binding;

    private String asal;
    private String tujuan;
    private String tanggal;
    private String jam;
    private String username;
    private String nomorKursi;
    private String order_id;


    private Context context;
    private String ukuranMobil;
    private int harga;
    private int jumlah;

    public TerimakasihFragment(){ }

    public static TerimakasihFragment newInstance(int harga, String username){
        TerimakasihFragment terimakasihFragment = new TerimakasihFragment();
        Bundle args = new Bundle();
        args.putInt("price", harga);
        args.putString("username" , username);

        terimakasihFragment.setArguments(args);
        return terimakasihFragment;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState){
        this.binding = FragmentTerimakasihBinding.inflate(inflater, container, false);
        View view = binding.getRoot();

        this.harga = this.getArguments().getInt("price");
        this.username = this.getArguments().getString("username");
        this.changeAllFont();
        this.binding.idHomeThanksBTN.setOnClickListener(this);
        this.showGIF();

        this.setText();
        this.context = this.getContext();
        return view;
    }

    private void showGIF(){
        Glide.with(this)
                .load("https://media.giphy.com/media/EAR3lTtsees9H8kYPG/giphy.gif")
                .diskCacheStrategy(DiskCacheStrategy.ALL)
                .circleCrop()
                .into(this.binding.gif);
    }

    private void setText(){
        getParentFragmentManager().setFragmentResultListener("kirimKeTerimakasih", this, new FragmentResultListener() {
            @Override
            public void onFragmentResult(@NonNull String requestKey, @NonNull Bundle result) {
                Course course = (Course) Parcels.unwrap(result.getParcelable("course"));
                asal = course.getSource();
                tujuan = course.getDestination();
                tanggal = result.getString("tanggal");
                jam = result.getString("jam");
                jumlah = result.getInt("jumlah");
                nomorKursi = result.getString("nomorKursi");
                HasilOrder hasilOrder = (HasilOrder) Parcels.unwrap(result.getParcelable("hasilOrder"));
                order_id = hasilOrder.getOrder_id();

                Log.d("COBA COBA DI TERIMAKAS", "BUNDLE KIRIM KE TERIMAKASI: " +
                        asal + " " + tujuan + " " + tanggal + " " + jam + " " + harga + " ");
                String all = "Terima kasih, " + username + "\n" +
                        "atas pemesanan travel " + "\n" +" pada tanggal " + tanggal + " jam " + jam + ".00" + "\n" +
                        " dari/tujuan" + "\n" + asal + " - " + tujuan + "\n" +
                        "dengan nomor kursi " + nomorKursi + "\n" +
                        " seharga Rp. " + jumlah;
                binding.TVThanks.setText(all);
                binding.tvOrderId.setText("Order ID: " + order_id);
            }
        });
    }

    @Override
    public void onClick(View view) {
        if(view == this.binding.idHomeThanksBTN){
            FragmentManager fragmentManager = this.getParentFragmentManager();
            Bundle result = new Bundle();
            result.putInt("page", 1);
            fragmentManager.setFragmentResult("changePage", result);
        }
    }
    public void changeAllFont(){
        this.binding.TVThanks.setTypeface(EasyFonts.robotoBold(this.getContext()));
        this.binding.tvOrderId.setTypeface(EasyFonts.robotoRegular(this.getContext()));
        this.binding.rednote.setTypeface(EasyFonts.robotoBold(this.getContext()));

        //button
        this.binding.idHomeThanksBTN.setTypeface(EasyFonts.caviarDreamsBold(this.getContext()));
    }
}
