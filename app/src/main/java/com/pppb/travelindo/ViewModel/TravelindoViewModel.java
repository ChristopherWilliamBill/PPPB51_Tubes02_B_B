package com.pppb.travelindo.ViewModel;

import android.os.Bundle;
import android.util.Log;
import android.widget.Toast;

import androidx.fragment.app.FragmentManager;
import androidx.lifecycle.MutableLiveData;
import androidx.lifecycle.ViewModel;

import com.pppb.travelindo.Model.Order;
import com.pppb.travelindo.Model.PayloadCourses;
import com.pppb.travelindo.Model.PayloadHistory;
import com.pppb.travelindo.Model.PayloadRoutes;
import com.pppb.travelindo.Model.User;
import com.pppb.travelindo.Network.RetroInstance;
import com.pppb.travelindo.Network.TravelAPI;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;

public class TravelindoViewModel extends ViewModel {
    private MutableLiveData<User> userMutableLiveData;
    private MutableLiveData<PayloadRoutes> payloadRoutesMutableLiveData;
    private MutableLiveData<PayloadCourses> payloadCoursesMutableLiveData;
    private MutableLiveData<PayloadHistory> payloadHistoryMutableLiveData;
    private MutableLiveData<Order> orderMutableLiveData;

    public TravelindoViewModel() {
        userMutableLiveData = new MutableLiveData<>();
        payloadRoutesMutableLiveData = new MutableLiveData<>();
        payloadCoursesMutableLiveData = new MutableLiveData<>();
        orderMutableLiveData = new MutableLiveData<>();
        payloadHistoryMutableLiveData = new MutableLiveData<>();
    }

    public MutableLiveData<User> getUserMutableLiveData(){
        return userMutableLiveData;
    }

    public MutableLiveData<PayloadHistory> getPayloadHistoryMutableLiveData(){ return payloadHistoryMutableLiveData; }

    public MutableLiveData<PayloadRoutes> getRoutesMutableLiveData(){ return payloadRoutesMutableLiveData; }

    public MutableLiveData<PayloadCourses> getCourseMutableLiveData(){ return payloadCoursesMutableLiveData; }

    public MutableLiveData<Order> getOrderMutableLiveData(){ return orderMutableLiveData; }

    public void addNewOrder(String token, String course_id, String seats){
        String auth = "Bearer " + token;
        TravelAPI travelAPI = RetroInstance.getRetroInstance().create(TravelAPI.class);
        Call<Order> call = travelAPI.addNewOrder(auth, course_id, seats);
        call.enqueue(new Callback<Order>() {
            @Override
            public void onResponse(Call<Order> call, Response<Order> response) {
                if(!response.isSuccessful()){
                    Log.d("Response", "failed");
                    orderMutableLiveData.postValue(null);
                    return;
                }
                orderMutableLiveData.postValue(response.body());
            }

            @Override
            public void onFailure(Call<Order> call, Throwable t) {
                orderMutableLiveData.postValue(null);
                Log.d("Response", t.getMessage());
            }
        });
    }

    public void getHistory(String token, int limit, int offset){
        String auth = "Bearer " + token;
        TravelAPI travelAPI = RetroInstance.getRetroInstance().create(TravelAPI.class);
        Call<PayloadHistory> call = travelAPI.getOrders(auth, limit, offset);
        call.enqueue(new Callback<PayloadHistory>() {
            @Override
            public void onResponse(Call<PayloadHistory> call, Response<PayloadHistory> response) {
                if(!response.isSuccessful()){
                    Log.d("Course gagal", response.message());
                    payloadHistoryMutableLiveData.postValue(null);
                    return;
                }
                payloadHistoryMutableLiveData.postValue(response.body());
            }

            @Override
            public void onFailure(Call<PayloadHistory> call, Throwable t) {
                payloadHistoryMutableLiveData.postValue(null);
                Log.d("Course failure", t.getMessage());
            }
        });
    }


    public void logIn(String username, String password){
        TravelAPI travelAPI = RetroInstance.getRetroInstance().create(TravelAPI.class);
        Call<User> call = travelAPI.logIn(username, password);
        call.enqueue(new Callback<User>() {
            @Override
            public void onResponse(Call<User> call, Response<User> response) {
                if(!response.isSuccessful()){
                    Log.d("Response", "failed");
                    userMutableLiveData.postValue(null);
                    return;
                }

                userMutableLiveData.postValue(response.body());
            }

            @Override
            public void onFailure(Call<User> call, Throwable t) {
                userMutableLiveData.postValue(null);
                Log.d("Response", t.getMessage());
            }
        });
    }

    public void getCourse(String token, String kotaAsal, String kotaTujuan, String ukuranMobil, String tanggal, String jam){
        String auth = "Bearer " + token;
        TravelAPI travelAPI = RetroInstance.getRetroInstance().create(TravelAPI.class);
        Call<PayloadCourses> call = travelAPI.getCourses(auth, kotaAsal, kotaTujuan, ukuranMobil, tanggal, jam);
        call.enqueue(new Callback<PayloadCourses>() {
            @Override
            public void onResponse(Call<PayloadCourses> call, Response<PayloadCourses> response) {
                if(!response.isSuccessful()){
                    Log.d("Course gagal", response.message());
                    payloadCoursesMutableLiveData.postValue(null);
                    return;
                }
                payloadCoursesMutableLiveData.postValue(response.body());
            }

            @Override
            public void onFailure(Call<PayloadCourses> call, Throwable t) {
                payloadCoursesMutableLiveData.postValue(null);
                Log.d("Course failure", t.getMessage());
            }
        });
    }

    public void getRoutes(String token){
        String auth = "Bearer " + token;
        TravelAPI travelAPI = RetroInstance.getRetroInstance().create(TravelAPI.class);
        Call<PayloadRoutes> call = travelAPI.getRoutes(auth);
        call.enqueue(new Callback<PayloadRoutes>() {
            @Override
            public void onResponse(Call<PayloadRoutes> call, Response<PayloadRoutes> response) {
                if(!response.isSuccessful()){
                    Log.d("Route gagal", response.message());
                    payloadRoutesMutableLiveData.postValue(null);
                    return;
                }
                payloadRoutesMutableLiveData.postValue(response.body());
            }

            @Override
            public void onFailure(Call<PayloadRoutes> call, Throwable t) {
                payloadRoutesMutableLiveData.postValue(null);
                Log.d("Route failure", t.getMessage());
            }
        });

    }

}
